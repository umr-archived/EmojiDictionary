//
//  EmojiTableViewController.swift
//  EmojiDictionary
//
//  Created by umar on 10/9/17.
//  Copyright © 2017 iOS. All rights reserved.
//

import UIKit

class EmojiTableViewController: UITableViewController {
    
    @IBAction func EditButtonTapped(_ sender: UIBarButtonItem) {
        let tableViewEditingMode = tableView.isEditing
        tableView.setEditing(!tableViewEditingMode, animated: true)
        
    }
    
    
    var emojis: [Emoji] =
        [Emoji(symbol:"😀", name: "grinning face", description: "A typical smiley face", usage: "happyness"),
         Emoji(symbol: "😕", name:"confused face", description: "A confused", usage: "unsure what do you think"),
         Emoji(symbol: "😍", name: "Heart Eyes", description: "A smiley face with heart eyes", usage: "love, attractive"),
         Emoji(symbol: "👮🏻", name: "Police officer", description: "police wearing blue cap", usage: "person authotiry"),
         Emoji(symbol: "🐢", name: "turtle", description: "A cute turtle" , usage: "Something slow"),
         Emoji(symbol: "🐘", name: "elephant", description: "A gray elephant", usage: "good memory"),
         Emoji(symbol: "🍝", name: "spagheti", description: "A plate of spagheti", usage: "spagheti"),
         Emoji(symbol: "🎲", name: "A single die", description: "single die", usage: "chance game"),
         Emoji(symbol: "⛺️", name: "tent", description: "A small tent", usage: "camping"),
         Emoji(symbol: "📚", name: "stack of books", description: "for before sleep reading", usage: "studying"),
         Emoji(symbol: "💔", name: "Broken heart", description: "A red, Broken heart", usage: "extreme sadness"),
         Emoji(symbol: "💤", name: "snore", description: "Three blue z", usage: "tired"),
         Emoji(symbol: "🏁", name: "checkered flag", description: "A black-white flag", usage: "completion")]
    
    override func viewWillAppear(_ animated: Bool) {
        tableView.reloadData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        //self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        //self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    
    
    @IBAction func unwindToEmojiTableView(for unwindSegue: UIStoryboardSegue, towardsViewController subsequentVC: UIViewController) {
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {

        return 1
    }
    
    
    // mengatur ada berapa row dalam setiap section
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if section == 0 {
            return emojis.count
        }else{
            return 0
        }
       // return emojis.count
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //dequeue cell
        let cell = tableView.dequeueReusableCell(withIdentifier: "EmojiCell", for: indexPath) as! EmojiTableViewCell
        
        //fetch model object to display
        let emoji = emojis[indexPath.row]
        
        
//        cell.textLabel?.text = "\(emoji.symbol) - \(emoji.name)"
//        cell.detailTextLabel?.text = emoji.description
    
        //configure cell
        cell.update(with: emoji)
        cell.showsReorderControl = true
        
        //return cell
        return cell
    }
    
    
    

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let emoji = emojis[indexPath.row]
        print("\(emoji.symbol) \(indexPath)")
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "EditEmoji" {
            let indexPath = tableView.indexPathForSelectedRow!
            let emoji = emojis[indexPath.row]
            let addEmojiEditTableViewController = segue.destination as! AddEditEmojiTableViewController
            addEmojiEditTableViewController.emoji = emoji
        }
    }
    
   
    
    override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let movedEmoji = emojis.remove(at: sourceIndexPath.row)
        emojis.insert(movedEmoji, at: destinationIndexPath.row)
        tableView.reloadData()
    }
    
    
    override func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .delete
    }
    
    

    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */


    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            emojis.remove(at: indexPath.row)
            let emoji = emojis[indexPath.row]
            tableView.deleteRows(at: [indexPath], with: .automatic)
            print("deleted emoji at \(emoji.symbol) \(indexPath)")
//        } else if editingStyle == .insert {
//            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
